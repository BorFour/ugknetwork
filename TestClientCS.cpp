/**
* @file TestClient.cpp
* @author Borja Mauricio Fourquet Maldonado
* @version 2017-02
* @date 2017-02-27
*/

#include <iostream>
#include "UGKNetwork.h"
#include "UGKNetworkConnection.h"
#include "UGKNetworkClientServer.h"
using namespace std;
using namespace UGK;


#define TARGET_PORT 27081
#define TARGET_IP "127.0.0.1"

void TestClient1()
{
	CClientCS cliente = CClientCS(TARGET_IP, TARGET_PORT);
	cout << "Conectando cliente" << endl;
	cliente.Init();
	for (;;)
	{
		cout << cliente.Receive() << endl;
	}

}


void TestClient2()
{
	CNetworkManager gestor = CNetworkManager();
	CClientCS cliente = CClientCS(TARGET_IP, TARGET_PORT);
	std::string msg;
	gestor.AddConnection(0, &cliente);
	cout << "Conectando cliente" << endl;
	cliente.Init();
	for (;;)
	{
		msg = gestor.ReceiveFromConnection(0);
		if (msg.size() > 0)
		{
			cout << msg << endl;
		}
	}

}

int main2()
{
	TestClient1();
	return 0;
}

