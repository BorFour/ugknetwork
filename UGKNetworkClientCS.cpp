/**
* @file UGKNetworkClientCS.cpp
* @author Borja Mauricio Fourquet Maldonado
* @version 2017-02
* @date 2017-02-27
*/

#include "UGKNetworkClientServer.h"
#include "UGKNetwork.h"
#include <string>

namespace UGK {

	/**
	*
	* @brief Constructor de la conexi�n CClientCS
	*
	* Esta clase sobrecarga el constructor de la clase virtual CNetworkConnection
	* tomando los mismos argumentos
	* @param ip Cadena de caracteres con la direcci�n IP a la que se conectar� el objeto posteriormente
	* @param ip N�mero entero con el puerto al que se conectar� el objeto posteriormente
	* @return Instancia de la clase CClientCS
	* @see CClientCS::Init
	*/

	CClientCS::CClientCS(std::string ip, int port) : CNetworkConnection(ip, port)
	{
		this->ip = ip;
		this->port = port;
	}

	/**
	*
	* @brief Inicializa los recursos de la conexi�n
	*
	* Verifica la versi�n del dll. Luego, crea un socket seg�n la ip y el puerto que deben estar almacenados en el objeto.
	* Si la versi�n es correcta, crea un socket y se conecta a la direcci�n anterior.
	* @code{.cpp}
	* std::string IP = "127.0.0.1";
	* int PORT = 1234;
	* CClientCS *conn_client = new CServerCS(IP, PORT);
	* conn_client->Init();
	* // El client ya est� listo para enviar y recibir mensajes
	* @endcode
	* @return C�digo de error
	*/

	int CClientCS::Init()
	{
		//Winsock Startup
		WSAData wsaData;
		WORD DllVersion = MAKEWORD(2, 1);
		if (WSAStartup(DllVersion, &wsaData) != 0) //If WSAStartup returns anything other than 0, then that means an error has occured in the WinSock Startup.
		{
			MessageBoxA(NULL, "Winsock startup failed", "Error", MB_OK | MB_ICONERROR);
			return 0; // exit(1);
		}

		SOCKADDR_IN addr; //Address to be binded to our Connection socket
		int sizeofaddr = sizeof(addr); //Need sizeofaddr for the connect function
									   //addr.sin_addr.s_addr = inet_addr("127.0.0.1"); //Address = localhost (this pc)
		addr.sin_addr.s_addr = 0L;
		inet_pton(AF_INET, ip.c_str(), &(addr.sin_addr)); //Address = localhost (this pc)
		addr.sin_port = htons(port); //Port = 1111
		addr.sin_family = AF_INET; //IPv4 Socket

		conn = socket(AF_INET, SOCK_STREAM, NULL); //Set Connection socket
		if (connect(conn, (SOCKADDR*)&addr, sizeofaddr) != 0) //If we are unable to connect...
		{
			MessageBoxA(NULL, "Failed to Connect", "Error", MB_OK | MB_ICONERROR);
			return 0; // 0; //Failed to Connect
		}
		//std::cout << "Connected!" << std::endl;
		return 0;
	}
	
	/**
	*
	* @brief Env�a un mensaje 
	*
	* Utiliza el socket previamente abierto para enviar un mensaje al servidor
	* @code{.cpp}
	* std::string IP = "127.0.0.1";
	* int PORT = 1234;
	* std::string msg = "Broadcast a todos los clientes";
	* CServerCS *conn_server = new CServerCS(IP, PORT);
	* conn_server->Init();
	* conn_server->AcceptConnection();
	* conn_server->Send(msg);
	* @endcode
	* @param msg Mensaje a enviar
	* @return C�digo de error
	* @see CClientCS::Receive
	*/

	int CClientCS::Send(std::string msg)
	{
		if (sizeof(msg) <= 0)
		{
			return ERR;
		}
		strcpy_s(buffer_out, msg.c_str());
		send(conn, buffer_out, sizeof(msg), NULL);
		return OK;
	}
		
	/**
	 *
	 * @brief Recibe un mensaje a trav�s del socket
	 *
	 * Utiliza el socket previamente abierto para enviar un mensaje al servidor
	 * @code{.cpp}
	 * std::string IP = "127.0.0.1";
	 * int PORT = 1234;
	 * std::string msg;
	 * CServerCS *conn_server = new CServerCS(IP, PORT);
	 * conn_server->Init();
	 * conn_server->AcceptConnection();
	 * msg = conn_server->Receive(msg);
	 * // Hacer algo con el string recibido
	 * @endcode
	 * @param msg Mensaje a enviar
	 * @return C�digo de error
	 * @see CClientCS::Receive
	 */

	std::string CClientCS::Receive(void)
	{
		recv(conn, buffer_in, BUFFER_SIZE, NULL);
		return buffer_in;
	}

	/// TODO
	void CClientCS::Update(void)
	{

	}

	/**
	*
	* @brief Libera los recursos utilizados en la conexi�n
	*
	* El m�todo cierra el socket previamente abierto por init()
	* @see CNetworkConnection::Exit
	* @see CClientCS::Init
	*/

	void CClientCS::Exit(void)
	{
		closesocket(conn);
		WSACleanup();
	}
}