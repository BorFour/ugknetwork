/**
 * @file CNetworkManagerCS.cpp
 * @author Borja Mauricio Fourquet Maldonado
 * @version 2017-02
 * @date 2017-02-27
 */

#include "UGKNetwork.h"
#include "UGKNetworkClientServer.h"
using namespace UGK;

namespace UGK {

	/**
	 *
	 * @brief Constructor del gestor de red
	 * @return Instancia de la clase CNetworkManager
	 */

	CNetworkManager::CNetworkManager(void)
	{
	
	}

	/**
	*
	* @brief A�ade una conexi�n al gestor de red
	*
	* La conexi�n ha de crearse antes de llamar a este m�todo,
	* y debe ser una instancia de una clase que implemente la clase virtual CNetworkConnection
	* @code{.cpp}
	* std::string IP = "127.0.0.1";
	* int PORT = 1234;
	* CServerCS *conn_server = new CServerCS(IP, PORT);
	* CClientCS *conn_client = new CClientCS(IP, PORT);
	* manager.AddConnection(0, conn_server);
	* manager.AddConnection(1, conn_client);
	* @endcode
	* @param idx Identificador num�rico de la conexi�n a a�adir el grupo
	* @param conn Puntero a un objeto de una clase que implemente CNetworkConnection
	* @return C�digo de error
	* @see CNetworkManager::RemoveConnection
	* @see CNetworkManager::EraseConnection
	*/

	int CNetworkManager::AddConnection(int idx, CNetworkConnection *conn)
	{
		connections[idx] = conn;
		return OK;
	}
	
	/**
	*
	* @brief Env�a un mensaje a una �nica conexi�n
	*
	* Accede a la conexi�n por la clave (id num�rico) y llama al m�todo Send()
	* @code{.cpp}
	* std::string IP = "127.0.0.1";
	* int PORT = 1234;
	* CNetworkManager manager = CNetworkManager();
	* CClientCS *conn_client = new CClientCS(IP, PORT);
	* std::string msg = "Mensaje a enviar";
	* manager.AddConnection(27, conn_client);
	* msg = manager.SendToConnection(27, msg);
	* @endcode
	* @param idx Identificador num�rico de la conexi�n
	* @param msg Mensaje a enviar
	* @return C�digo de error
	* @see CNetworkManager::AddConnection
	* @see CNetworkManager::ReceiveFromConnection
	*/

	int CNetworkManager::SendToConnection(int idx, std::string msg)
	{
		return connections[idx]->Send(msg);
	}
	
	/**
	*
	* @brief Recibe un mensaje de una �nica conexi�n
	*
	* Accede a la conexi�n por la clave (id num�rico) y llama al m�todo Receive() 
	* @code{.cpp}
	* std::string IP = "127.0.0.1";
	* int PORT = 1234;
	* CNetworkManager manager = CNetworkManager();
	* CClientCS *conn_client = new CClientCS(IP, PORT);
	* std::string msg;
	* manager.AddConnection(27, conn_client);
	* msg = manager.ReceiveFromConnection(27);
	* // Hacer algo con el mensaje recibido
	* @endcode
	* @param idx Identificador num�rico de la conexi�n
	* @return C�digo de error
	* @see CNetworkManager::AddConnection
	* @see CNetworkManager::SendToConnection
	*/

	std::string CNetworkManager::ReceiveFromConnection(int idx)
	{
		return connections[idx]->Receive();
	}

	 /**
	 *
	 * @brief Elimina la referencia a una conexi�n del gestor de red
	 *
	 * La conexi�n debe estar previamente creada y a�adadida al gestor de red, al igual que el grupo de conexiones debe estar creado de antemano.
	 * @code{.cpp}
	 * int group_id = 2;
	 * manager.RemoveConnectionFromGroup(0, group_id);
	 * @endcode
	 * @param idx Identificador num�rico de la conexi�n
	 * @param group_id Identificador num�rico del grupo
	 * @return C�digo de error
	 * @see CNetworkManager::RemoveConnection
	 * @see CNetworkManager::AddConnection
	 */

	int CNetworkManager::RemoveConnection(int idx)
	{
		std::map<int, std::vector<CNetworkConnection*>>::iterator it;
		for (it = connection_groups.begin(); it != connection_groups.end(); it++)
		{
		
		}
		connections[idx] = NULL;
		return OK;	
	}

	 /**
	  *
	  * @brief Elimina la referencia y el objecto correspondiente a una conexi�n del gestor de red
	  *
	  * La conexi�n debe estar previamente creada y a�adida al gestor de red, al igual que el grupo de conexiones debe estar creado de antemano.
	  * @code{.cpp}
	  * CNetworkManager manager = CNetworkManager();
	  * std::string IP = "127.0.0.1";
	  * int PORT = 1234;
	  * CServerCS *conn_server = new CServerCS(IP, PORT);
	  * CClientCS *conn_client = new CClientCS(IP, PORT);
	  * manager.AddConnection(27, conn_client);
	  * manager.AddConnection(32, conn_client);
	  * // Despu�s de su uso
	  * manager.EraseConnection(27);
	  * manager.EraseConnection(32);
	  * @endcode
	  * @param idx Identificador num�rico de la conexi�n
	  * @param group_id Identificador num�rico del grupo
	  * @return C�digo de error
	  * @see CNetworkManager::RemoveConnection
	  * @see CNetworkManager::AddConnection
	  */

	int CNetworkManager::EraseConnection(int idx)
	{
		delete connections[idx];
		connections[idx] = NULL;
		return OK;
	}

	/**
	*
	* @brief Crea un grupo de conexiones
	*
	* Es recomendable utilizar grupos de conexiones para agrupar conexiones a las que se va a tratar exactamente igual
	* @code{.cpp} 
	* int group_id = 2;
	* std::set<int> indices;
	* indices.insert(0);
	* indices.insert(1);
	* indices.insert(2);
	* manager.CreateConnectionsGroup(group_id, indices);
	* @endcode
	* @param group_id Identificador num�rico del grupo
	* @param indices Conjunto de �ndices que se a�adir�n al grupo creado
	* @return C�digo de error
	* @see CNetworkManager::GetConnectionsGroup
	* @see CNetworkManager::AddConnectionToGroup
	* @see CNetworkManager::RemoveConnectionFromGroup
	*/

	int CNetworkManager::CreateConnectionsGroup(int group_id, std::set<int> indices)
	{
		std::vector<CNetworkConnection*> conns;
		for (std::map<int, CNetworkConnection*>::iterator it = connections.begin(); it != connections.end(); ++it) 
		{
				bool is_included = false;
				for each (int idx in indices)
				{
					if (it->first == idx)
					{
						is_included = true;
						conns.push_back(it->second);
					}
				}
		
		}
		connection_groups[group_id] = conns;
		return OK;
	}

	/**
	*
	* @brief A�ade una conexi�n a un grupo de conexiones
	*
	* La conexi�n debe estar previamente creada y a�adida al gestor de red, al igual que el grupo de conexiones debe estar creado de antemano.
	* @code{.cpp}
	* int group_id = 2;
	* manager.AddConnectionToGroup(3, group_id);
	* @endcode
	* @param idx Identificador num�rico de la conexi�n a a�adir el grupo
	* @param group_id Identificador num�rico del grupo
	* @return C�digo de error
	* @see CNetworkManager::GetConnectionsGroup
	* @see CNetworkManager::CreateConnectionsGroup
	* @see CNetworkManager::RemoveConnectionFromGroup
	*/

	int CNetworkManager::AddConnectionToGroup(int group_id, int idx)
	{
		connection_groups[group_id].push_back(connections[idx]);
		return OK;
	}

	/**
	*
	* @brief Env�a un mensaje a todos los miembros de un grupo
	*
	* Recorre el vector correspondiente al grupo y utiliza el m�todo Send() de las conexiones
	*
	* @param group_id Identificador num�rico del grupo
	* @param msg Mensaje a enviar
	* @return C�digo de error
	* @see CNetworkManager::CreateConnectionsGroup
	* @see CNetworkManager::ReceiveFromGroup
	*/

	int CNetworkManager::SendToGroup(int group_id, std::string msg)
	{
		for each (CNetworkConnection* var in connection_groups[group_id])
		{
			var->Send(msg);
		}
		return OK;
	}

	/**
	*
	* @brief Recibe un mensaje de cada uno de los miembros del grupo
	*
	* Recorre el vector correspondiente al grupo y utiliza el m�todo Receive() de las conexiones.
	* Almacena los resultados en un vector y lo devuelve
	*
	* @param group_id Identificador num�rico del grupo
	* @return C�digo de error
	* @see CNetworkManager::CreateConnectionsGroup
	* @see CNetworkManager::SendToGroup
	*/

	std::vector<std::string> CNetworkManager::ReceiveFromGroup(int group_id)
	{
		std::vector<std::string> result;
		for each (CNetworkConnection* var in connection_groups[group_id])
		{
			result.push_back(var->Receive());
		}
		return result;
	}

	/**
	*
	* @brief Elimina la referencia de una conexi�n dentro de un grupo
	*
	* La conexi�n debe estar previamente creada y a�adida al gestor de red, al igual que el grupo de conexiones debe estar creado de antemano.
	* @code{.cpp}
	* int group_id = 2;
	* manager.RemoveConnectionFromGroup(0, group_id);
	* @endcode
	* @param group_id Identificador num�rico del grupo
	* @param idx Identificador num�rico de la conexi�n a eliminar del grupo
	* @return C�digo de error
	* @see CNetworkManager::GetConnectionsGroup
	* @see CNetworkManager::CreateConnectionsGroup
	* @see CNetworkManager::AddConnectionToGroup
	*/

	int CNetworkManager::RemoveConnectionFromGroup(int group_id, int idx)
	{
		CNetworkConnection* con = connections[idx];

		if (con == NULL) return ERR;

		for (std::vector<CNetworkConnection*>::iterator it = connection_groups[group_id].begin(); it != connection_groups[group_id].end(); ++it)
		{
			if (con == *it)
			{
				connection_groups[group_id].erase(it);
			}
		}

		return OK;
	}
	
	/**
	*
	* @brief Libera las conexiones creadas por el servidor y los recursos utilizados
	*
	* Recorre el map con todas las conexiones y llama a su m�todo Exit() y las destruye.
	* Despu�s, vac�a los vectores correspondientes a los grupos
	*
	* @see CServerCS::Exit
	* @see CClientCS::Exit
	*/

	void CNetworkManager::Exit(void)
	{
		for (std::map<int, CNetworkConnection*>::iterator it = connections.begin(); it != connections.end(); ++it) {
			it->second->Exit();
			delete it->second;
		}

		for (std::map<int, std::vector<CNetworkConnection*>>::iterator it = connection_groups.begin(); it != connection_groups.end(); ++it) {
			it->second.clear();
		}
	}

	/// TODO
	void CNetworkManager::ParseNetworkConfig(char* HTMLFile)
	{

	}
	/// TODO
	void CNetworkManager::Initialize()
	{

	}
	/// TODO
	void CNetworkManager::StartThreads()
	{

	}
	/// TODO
	void CNetworkManager::Update()
	{

	}
	/// TODO
	void CNetworkManager::StopThreads()
	{

	}

}