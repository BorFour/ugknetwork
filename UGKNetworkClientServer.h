/**
 *
 * @file UGKNewtorkClientServer.h
 * @brief Clases (Cliente y Servidor) que implementan la clase virtual CNetworkConnection
 * @author Borja Mauricio Fourquet Maldonado
 * @version 2017-02
 * @date 2017-02-27
 *
 */

#pragma once

#include "UGKNetworkConnection.h"
#include <vector>

namespace UGK {

	class CClientCS : public CNetworkConnection
		{
		static const int BUFFER_SIZE = 1024;
		private:
			char buffer_in[BUFFER_SIZE];
			char buffer_out[BUFFER_SIZE];
		public:
			/// Socket propio de la conexión
			SOCKET conn;
			CClientCS(std::string ip, int port);
			virtual int Init(void) override;
			virtual int Send(std::string msg) override;
			virtual void Update() override;
			virtual std::string Receive(void) override;
			virtual void Exit(void) override;
	};

	class CServerCS : public CNetworkConnection
	{
		static const int BUFFER_SIZE = 1024;
		private:
			char buffer_in[BUFFER_SIZE];
			char buffer_out[BUFFER_SIZE];
			SOCKADDR_IN addr;
		public:
			/// Socket propio de la conexión
			SOCKET sListen;
			CServerCS(std::string ip, int port);
			std::vector<CClientCS*> connections;
			int GetNClients(void);
			virtual int Init(void) override;
			virtual int Send(std::string msg) override;
			virtual void Update() override;
			virtual std::string Receive(void) override;
			std::string	Receive(int conn_id);
			std::vector<std::string> CServerCS::ReceiveAll(void);
			CClientCS* AcceptConnection(void);
			virtual void Exit(void) override;
	};

}
