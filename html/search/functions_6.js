var searchData=
[
  ['receive',['Receive',['../class_u_g_k_1_1_c_client_c_s.html#aa6688efe3575ac183c9de9e1dfe1e3e2',1,'UGK::CClientCS::Receive()'],['../class_u_g_k_1_1_c_server_c_s.html#a4ac0ab45b3d74af5b847bc8c35f0ca56',1,'UGK::CServerCS::Receive(void) override'],['../class_u_g_k_1_1_c_server_c_s.html#a76435424b63ce374256b1b1b9d37da9e',1,'UGK::CServerCS::Receive(int conn_id)'],['../class_u_g_k_1_1_c_network_connection.html#a2752132cf791987c0ea3968b17a16b7a',1,'UGK::CNetworkConnection::Receive()']]],
  ['receivefromconnection',['ReceiveFromConnection',['../class_u_g_k_1_1_c_network_manager.html#a8fb91e62a03b47efa47cfbfa29de54bd',1,'UGK::CNetworkManager']]],
  ['receivefromgroup',['ReceiveFromGroup',['../class_u_g_k_1_1_c_network_manager.html#afa452f6c9628ad2aede6d5837531acd4',1,'UGK::CNetworkManager']]],
  ['removeconnection',['RemoveConnection',['../class_u_g_k_1_1_c_network_manager.html#ae92812c594b7f3d2476d0b5373536198',1,'UGK::CNetworkManager']]],
  ['removeconnectionfromgroup',['RemoveConnectionFromGroup',['../class_u_g_k_1_1_c_network_manager.html#ab6ab5e9645d640d00c3a4b1b0ab5434f',1,'UGK::CNetworkManager']]]
];
