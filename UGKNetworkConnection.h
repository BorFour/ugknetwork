/**
 *
 * @file UGKNewtorkConnection.h
 * @brief Clases virtual que representa la conexión entre dos máquinas
 * @author Borja Mauricio Fourquet Maldonado
 * @version 2017-02
 * @date 2017-02-27
 *
 */

#include "UGKNetwork.h"

#ifndef UGK_NET_CONN
#define UGK_NET_CONN

#pragma comment(lib,"ws2_32.lib")
#include <WinSock2.h>
#include <iostream>
#include <Ws2tcpip.h>
#include <string>
#define DEFAULT_PORT "27015"
#define DEFAULT_IP "127.0.0.1"

namespace UGK
{
	class CNetworkConnection
	{
		protected:
			/// Dirección ip a la que se conecta
			std::string ip;
			/// Puerto al que se conecta
			int port;
		public:
			CNetworkConnection(std::string ip, int port);
			/// Función de inicialización de la conexión
			virtual int Init(void) = 0;
			/// Función para enviar un mensaje a través del canal de comunicación
			virtual int Send(std::string msg) = 0;
			/// Función que actualiza la conexión (aceptar nuevas conexiones)
			virtual void Update() = 0;
			/// Función para leer mensajes del canal de comuniación (¿asíncrono?)
			virtual std::string Receive(void) = 0;
			/// Función que se ha de ejecutar al terminar la conexión
			virtual void Exit(void) = 0;
	};
}

#endif
