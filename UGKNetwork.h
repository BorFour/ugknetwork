/**
 *
 * @file UGKNewtorkCS.h
 * @brief Clase genérica correspondiente a un gestor de red.
 * Principalmente, tiene una serie de atributos que definen su arquitectura y se encarga de gestionar cada conexión con otras máquinas remotas.
 * @author Borja Mauricio Fourquet Maldonado
 * @version 2017-02
 * @date 2017-02-27
 *
 */

#ifndef UGK_NET
#define UGK_NET

#include "UGKNetworkConnection.h"
#include "UGKNetworkClientServer.h"
#include <vector>
#include <set>
#include <map>
#include <memory>

/// Interfaz de red que utiliza la máquina actual
typedef enum {
	INTERFACE_CABLE = 0,
	INTERFACE_WIFI,
	INTERFACE_LOCALHOST,
	INTERFACE_MAX
} NET_INTERFACE;

/// Arquitectura de comunicación de la red: ¿quién debe enviar qué a quién?
typedef enum {
	CLIENT_SERVER = 0, /** Red Cliente-Servidor */
	P2P,
	SERVER_NETWORK,
	COMMUNICATION_ARCH_MAX
} NET_COMMUNICATION_ARCH;


/// Arquitectura de datos de la red: ¿dónde se almacena la información?
typedef enum {
	CENTRALIZED = 0,
	DISTRIBUTED,
	REDUNDANT,
	DATA_ARCH_MAX
} NET_DATA_ARCH;


/// Estrategias de sincronización entre distintas instancias del juego
typedef enum {
	DETERMINISTIC_LOCKSTEP = 0,
	SNAPSHOT_INTERPOLATION,
	STATE_SYNCHRONIZATION,
	SYNC_STRATEGY_MAX
} NET_SYNC_STRATEGY;

enum {
	ERR=-1,
	OK= 0
} ;


namespace UGK {
	class CNetworkManager
	{
		protected:
			NET_INTERFACE net_interface;
			NET_COMMUNICATION_ARCH communication_arch;
			NET_DATA_ARCH data_arch;
			NET_SYNC_STRATEGY sync_strategy;
			/// Mapa de conexiones. Cada conexión representa un stream unidireccional de datos con otra máquina.
			std::map<int, CNetworkConnection*> connections;
			std::map<int, std::vector<CNetworkConnection*>> connection_groups;
		public:
			CNetworkManager(void);
			/// Frecuencia de actualización
			int refresh_rate;
			inline NET_INTERFACE GetInterface() { return net_interface; }
			inline NET_COMMUNICATION_ARCH GetCommunicationArch() { return communication_arch; }
			inline NET_DATA_ARCH GetDataArch() { return data_arch; }
			inline NET_SYNC_STRATEGY GetSyncStrategy() { return sync_strategy; }
			inline std::map<int, CNetworkConnection*> GetConnections() { return connections; }
			/// Método de acceso al grupo de conexiones
			inline std::vector<CNetworkConnection*> GetConnectionsGroup(int group_id) { return connection_groups[group_id]; }
			/// Añade una conexión al gestor de conexiones
			int AddConnection(int idx, CNetworkConnection* conn);
			/// asdfasdfasdf
			int SendToConnection(int idx, std::string msg);
			/// sdfgsdfgsdf
			std::string ReceiveFromConnection(int idx);
			/// Elimina la referencia a una conexión
			int RemoveConnection(int idx);
			/// Elimina una conexión según su identificador (int idx) del gestor de conexiones
			int EraseConnection(int idx);
			/// Agrupa un conjunto de conexiones previamente creadas
			int CreateConnectionsGroup(int group_id, std::set<int> indices);
			/// Añade una conexión a un grupo
			int AddConnectionToGroup(int group_id, int idx);
			/// Envía un mensaje a través de todas las conexiones de un grupo
			int SendToGroup(int group_id, std::string msg);			
			/// Recibe un mensaje de cada una de las conexiones de un grupo
			std::vector<std::string> ReceiveFromGroup(int group_id);
			/// Elimina la referencia a una conexión de un grupo
			int RemoveConnectionFromGroup(int group_id, int idx);
			/// Carga la topología de la red de un archivo HTML
			void ParseNetworkConfig(char* HTMLFile);
			/// Inicializa los recursos de la clase
			void Initialize();
			/// Crea el hilo/los hilos de recepción/envío de datos
			void StartThreads();
			/// Método que ha de ejecutarse periódicamente, según la frecuencia refresh_rate
			void Update();
			/// Finaliza el hilo/los hilos de recepción/envío de datos
			void StopThreads();

			void Exit();
	};
}
#endif
