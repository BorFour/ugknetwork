
#include <iostream>
#include "UGKNetwork.h"
#include "UGKNetworkConnection.h"
#include "UGKNetworkClientServer.h"
using namespace std;
using namespace UGK;


#define TARGET_PORT 27081
#define TARGET_IP "127.0.0.1"

int main()

{
	CNetworkManager nm = CNetworkManager();
	CClientCS con = CClientCS();
	nm.AddConnection(0, &con);
	cout << "Connecting client" << endl;
	con.init();
	cout << con.receive() << endl;
	system("pause");
	return 0;
}
