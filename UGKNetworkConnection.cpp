/**
* @file UGKNetworkConnection.cpp
* @author Borja Mauricio Fourquet Maldonado
* @version 2017-02
* @date 2017-02-27
*/

#include "UGKNetworkConnection.h"

namespace UGK {

	/**
	 *
	 * @brief Constructor de la conexi�n CNetworkConnection
	 *
	 * Este m�todo pertenece a una clase virtual.
	 * NUNCA deber�a ser llamado. Llamar a constructores de clases que sobrecarguen el m�todo
	 * @param ip Cadena de caracteres con la direcci�n IP en la que el objeto aceptar� conexiones posteriormente
	 * @param ip N�mero entero con el puerto en el que aceptar� conexiones posteriormente
	 * @return Instancia de la clase CServerCS
	 * @see CServerCS::Init
	 * @warning S�lo utilizar en constructores de clases que extiendan a CNetworkConnection
	 */

	CNetworkConnection::CNetworkConnection(std::string ip, int port)
	{
		this->ip = ip;
		this->port = port;
	}

}