/**
* @file UGKNetworkServerCS.cpp
* @author Borja Mauricio Fourquet Maldonado
* @version 2017-02
* @date 2017-02-27
*/

#include "UGKNetworkClientServer.h"
#include "UGKNetwork.h"
#include <string>

namespace UGK 
{
	/**
	*
	* @brief Constructor de la conexi�n CServerCS
	*
	* Esta clase sobrecarga el constructor de la clase virtual CNetworkConnection
	* tomando los mismos argumentos
	* @param ip Cadena de caracteres con la direcci�n IP en la que el objeto aceptar� conexiones posteriormente
	* @param ip N�mero entero con el puerto en el que aceptar� conexiones posteriormente
	* @return Instancia de la clase CServerCS
	* @see CServerCS::Init
	*/

	CServerCS::CServerCS(std::string ip, int port) : CNetworkConnection(ip, port)
	{
		this->ip = ip;
		this->port = port;
	}

	/**
	*
	* @brief Inicializa los recursos del servidor
	*
	* Verifica la versi�n del dll. Luego, crea un socket seg�n la ip y el puerto que deben estar almacenados en el objeto.
	* Si la versi�n es correcta, hace un bind() y un listen() en la direcci�n anterior
	* @code{.cpp}
	* std::string IP = "127.0.0.1";
	* int PORT = 1234;
	* CServerCS *conn_server = new CServerCS(IP, PORT);
	* conn_server->Init();
	* // El servidor ya est� listo para aceptar conexiones
	* @endcode
	* @return C�digo de error
	* @see CNetworkConnection::Init
	* @see CServerCS::AcceptConnection
	*/

	int CServerCS::Init()
	{
		// WinSock Startup
		WSAData wsaData;
		WORD DllVersion = MAKEWORD(2, 1);
		if (WSAStartup(DllVersion, &wsaData) != 0) 
		{
			MessageBoxA(NULL, "WinSock startup failed", "Error", MB_OK | MB_ICONERROR);
			return ERR;
		}

		int addrlen = sizeof(addr); // Longitud de la direcci�n
		addr.sin_addr.s_addr = 0L;
		inet_pton(AF_INET, ip.c_str(), (void*)addr.sin_addr.s_addr); // Direcci�n
		addr.sin_port = htons(port); // Puerto
		addr.sin_family = AF_INET; // Socket IPv4

		sListen = socket(AF_INET, SOCK_STREAM, NULL); // Crear un socket para aceptar conexiones
		bind(sListen, (SOCKADDR*)&addr, sizeof(addr)); // Enlazar la direcci�n con el socket
		listen(sListen, SOMAXCONN); // Desplegar el socket para que escuche nuevas conexiones
		
		return OK;
	}


	/**
	*
	* @brief Env�a un mensaje a todas las conexiones del servidor
	*
	* Recorre el vector de conexiones y utiliza el m�todo submit() de estas para enviar el mismo mensaje a todas
	* @code{.cpp}
	* std::string IP = "127.0.0.1";
	* int PORT = 1234;
	* std::string msg = "Broadcast a todos los clientes";
	* CServerCS *conn_server = new CServerCS(IP, PORT);
	* conn_server->Init();
	* conn_server->AcceptConnection();
	* conn_server->Send(msg);
	* @endcode
	* @param msg Mensaje a enviar a trav�s de las conexiones
	* @return C�digo de error
	* @see CServerCS::AcceptConnection
	*/

	int CServerCS::Send(std::string msg)
	{
		for (std::vector<CClientCS*>::iterator it = connections.begin(); it != connections.end(); ++it) {
			(*it)->Send(msg);
		}	
		return 0;
	}

	/// TODO
	std::string CServerCS::Receive(void)
	{
		return "";
	}

	/// TODO
	std::string CServerCS::Receive(int conn_id)
	{
		return "";
	}

	/// TODO
	std::vector<std::string> CServerCS::ReceiveAll(void)
	{
		std::vector<std::string> ret;
		for (std::vector<CClientCS*>::iterator it = connections.begin(); it != connections.end(); ++it) {
			ret.push_back((*it)->Receive());
		}
		return ret;
	}

	/**
	 *
	 * @brief Devuelve el n�mero de clientes en este objeto
	 *
	 * Devuelve el tama�o del vector en el que se almacenan las conexiones.
	 * @return N�mero de clientes conectados al servidor
	 * @see CServerCS::AcceptConnection
	 */

	int CServerCS::GetNClients(void)
	{
		return connections.size();
	}

	/**
	 *
	 * @brief Acepta una conexi�n del socket asociado a este objeto
	 *
	 * Este m�todo es pr�cticamente no bloqueante. La conexi�n tipo CClientCS se a�ade al vector de conexiones del servidor,
	 * pero tambi�n se devuelve su puntero 
	 * @code{.cpp}
	 * std::string IP = "127.0.0.1";
	 * int PORT = 1234;
	 * CServerCS *conn_server = new CServerCS(IP, PORT);
	 * CClientCS *conn_client = conn_server->accept_connection();
	 * if(conn_client != NULL) {
	 * // Hacer algo con la nueva conexi�n
	 * }
	 * @endcode
	 * @return Puntero al nuevo objeto con la conexi�n aceptada o NULL en caso de no poder establecerse
	 * @see CNetworkManager::GetConnectionsGroup
	 */

	CClientCS* CServerCS::AcceptConnection(void)
	{

		int addrlen = sizeof(addr); // Longitud de la direcci�n
		SOCKET newConnection; // Socket que soportar� la nueva conexi�n
		CClientCS * cli; // Conexi�n tipo cliente que devolver� la funci�n
		fd_set readSet; 
		FD_ZERO(&readSet);
		FD_SET(sListen, &readSet);
		timeval timeout;
		timeout.tv_sec = 0;  // Zero timeout (poll)
		timeout.tv_usec = 0;

		if (select(sListen, &readSet, NULL, NULL, &timeout) == 1)
		{
			newConnection = accept(sListen, (SOCKADDR*)&addr, &addrlen); // Aceptar la nueva conexi�n
			char MOTD[256] = "Welcome! This is the Message of the Day."; // Crear buffer para enviar el mensaje del d�a
			send(newConnection, MOTD, sizeof(MOTD), NULL); // Enviar el mensaje del d�a a la conexi�n creada
			cli = new CClientCS("", 0);
			cli->conn = newConnection;
			connections.push_back(cli);
			return cli;
		}		

		return NULL;
	}

	/// TODO
	void CServerCS::Update(void)
	{

	}

	/**
	*
	* @brief Libera las conexiones creadas por el servidor y los recursos utilizados
	*
	* Este m�todo recorre el vector de conexiones, llamando al m�todo exit() de cada conexi�n
	* y cierra el socket en el que aceptaba conexiones
	* @see CNetworkConnection::Exit
	* @see CClientCS::Exit
	*/

	void CServerCS::Exit(void)
	{
		closesocket(sListen);
		WSACleanup();
		for each (CClientCS* var in connections)
		{
			var->Exit();
			delete var;
		}
	}

}
