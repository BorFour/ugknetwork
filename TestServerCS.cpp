/**
* @file TestServer.cpp
* @author Borja Mauricio Fourquet Maldonado
* @version 2017-02
* @date 2017-02-27
*/

#include <iostream>
#include "UGKNetwork.h"
#include "UGKNetworkConnection.h"
#include "UGKNetworkClientServer.h"
#include <sstream>
using namespace std;
using namespace UGK;


#define TARGET_PORT 27081
#define TARGET_IP "127.0.0.1"


void TestServer1()
{
	CServerCS servidor = CServerCS(TARGET_IP, TARGET_PORT);
	cout << "Conectando servidor" << endl;
	servidor.Init();
	vector<CNetworkConnection> conexiones;
	for (;;)
	{
		if (servidor.AcceptConnection() != NULL)
		{
			std::stringstream ss;
			ss << "Clientes conectados: " << servidor.GetNClients() << endl;
			std::cout << ss.str();
			servidor.Send(ss.str());
		}		
	}

}

void TestServer2()
{
	CNetworkManager gestor = CNetworkManager();
	CServerCS servidor = CServerCS(TARGET_IP, TARGET_PORT);
	vector<CNetworkConnection> conexiones;
	gestor.AddConnection(0, &servidor);

	cout << "Conectando servidor" << endl;
	servidor.Init();
	gestor.CreateConnectionsGroup(1, {});

	for (;;)
	{
		CClientCS *nuevo_cliente = servidor.AcceptConnection();
		int indice_nuevo_cliente = servidor.GetNClients() + 1;
		std::stringstream ss;

		if (nuevo_cliente != NULL)
		{
			ss << "Clientes conectados: " << servidor.GetNClients() << endl;
			gestor.AddConnection(indice_nuevo_cliente, nuevo_cliente);
			gestor.AddConnectionToGroup(1, indice_nuevo_cliente);
			gestor.SendToGroup(1, ss.str());
		}
	}

}

int main()
{
	TestServer2();
	return 0;
}

