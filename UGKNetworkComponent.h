#pragma once

#include "UGKNetwork.h"
namespace UGK
{
	class CNetworkComponent
	{
		protected:
			NET_INTERFACE interface;
			NET_COMMUNICATION_ARCH communication_arch;
			NET_DATA_ARCH data_arch;
			NET_SYNC_STRATEGY sync_strategy;
		public:
			vector<CNetworkConnection> conns;
	};	
}
